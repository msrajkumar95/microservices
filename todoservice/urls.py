from django.contrib import admin
from django.urls import path
from todoservice.views import ToDoListView, ToDoDetailView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/todo', ToDoListView.as_view()),
    path('api/todo/<int:pk>', ToDoDetailView.as_view()),
]
