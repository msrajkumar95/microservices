from django.db import models


class ToDo(models.Model):

    username = models.CharField(max_length=100)
    venue = models.CharField(max_length=100)
    task = models.CharField(max_length=100)
