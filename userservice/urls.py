from django.contrib import admin
from django.urls import path
from userservice.views import UsersView, ViewUser, ModifyUser

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/users', UsersView.as_view()),
    path('api/user/view', ViewUser.as_view()),
    path('api/user/modify/<int:pk>', ModifyUser.as_view()),
]
